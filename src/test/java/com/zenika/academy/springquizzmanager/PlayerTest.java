package com.zenika.academy.springquizzmanager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PlayerTest {

    @Test
    void with0Points() {
        Player p = new Player("Jean");

        Assertions.assertEquals("Félicitations, Jean, vous avez marqué 0 points.", p.congratulations());
    }

    @Test
    void withScoredPoints() {
        Player p = new Player("Wolfgang");
        p.scorePoints(2);

        Assertions.assertEquals("Félicitations, Wolfgang, vous avez marqué 2 points.", p.congratulations());
    }

    @Test
    void withSeveralScoredPoints() {
        Player p = new Player("Luke");
        p.scorePoints(2);
        p.scorePoints(1);

        Assertions.assertEquals("Félicitations, Luke, vous avez marqué 3 points.", p.congratulations());
    }

    @Test
    void withScored0() {
        Player p = new Player("Leïa");
        p.scorePoints(2);
        p.scorePoints(0);

        Assertions.assertEquals("Félicitations, Leïa, vous avez marqué 2 points.", p.congratulations());
    }
}