package com.zenika.academy.springquizzmanager;

import com.zenika.academy.springquizzmanager.repositories.IdGeneratorConfig;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FactoryMovieQuestionTest {
   /*
    @Test
    void generateMovieFormTitleTest() throws IOException {
        TmdbClient tmdbClient = new TmdbClient(new File("src/main/resources/tmdb-api-key.txt"));
        AtomicLong id = new AtomicLong();
        FactoryMovieQuestion factoryMovieQuestion = new FactoryMovieQuestion(tmdbClient,id);
        assertEquals("Quel est le film sorti en 1997 dans lequel les acteurs Leonardo DiCaprio , Kate Winslet et Billy Zane ?",
                factoryMovieQuestion.generateOpenQuestion().get().getDisplayableText());
    }

    @Test
    void generatePersonnageFormTitleTestMock() {
        //arrange
        MovieInfo movieInfo = new MovieInfo("Titanic", Year.of(1997),
                List.of(new MovieInfo.Character("Jack", "Leonardo"),
                        new MovieInfo.Character("Rose", "Kate"),
                        new MovieInfo.Character("Molly", "Katy")));
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        AtomicLong id = new AtomicLong();
        when(mockTmdbClient.getMovie("Titanic")).thenReturn(Optional.of(movieInfo));
        FactoryMovieQuestion factoryMovieQuestion = new FactoryMovieQuestion(mockTmdbClient, id);
        //act
        Optional<Question> optionalQuestion = factoryMovieQuestion.generatePersonnageFromTitle("Titanic");
        //assert
        assertTrue(optionalQuestion.isPresent());
        assertEquals("Quel est le personnage interprété par Kate dans le film Titanic ?", optionalQuestion.get().getDisplayableText());
        assertEquals(com.zenika.academy.quiz.questions.AnswerResult.CORRECT, optionalQuestion.get().tryAnswer("Rose"));
    }
    @Test
    void generateActorFromTitleMock() {
        //arrange
        MovieInfo movieInfo = new MovieInfo("Star Wars : The Empire Strikes Back", Year.of(1977),
                List.of(new MovieInfo.Character("Luke Skywalker", "Mark Hamill"),
                        new MovieInfo.Character("Han Solo", "Harrison Ford"),
                        new MovieInfo.Character("Princesse Leia", "Carrie Fisher")));
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        AtomicLong id = new AtomicLong();
        when(mockTmdbClient.getMovie("Star Wars : The Empire Strikes Back")).thenReturn(Optional.of(movieInfo));
        FactoryMovieQuestion factoryMovieQuestion = new FactoryMovieQuestion(mockTmdbClient, id);
        //act
        Optional<Question> optionalQuestion = factoryMovieQuestion.generateActorFromTitle("Star Wars : The Empire Strikes Back");
        //assert
        assertTrue(optionalQuestion.isPresent());
        assertEquals("Quel acteur joue le personnage de Luke Skywalker dans le film Star Wars : The Empire Strikes Back ?", optionalQuestion.get().getDisplayableText());
        assertEquals(com.zenika.academy.quiz.questions.AnswerResult.CORRECT, optionalQuestion.get().tryAnswer("Mark Hamill"));
    }

    @Test
    void doitDonnerDesPoints () {
        //arrange

        //act

        //assert

    }*/
}

