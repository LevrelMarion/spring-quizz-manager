package com.zenika.academy.springquizzmanager;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TrueFalseQuestionTest {
/*
    @Test
    void correctTrueAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en bretagne", true, 2);

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("oui")),
                () -> assertEquals(CORRECT, q.tryAnswer("vRai")),
                () -> assertEquals(CORRECT, q.tryAnswer("True"))
        );
    }

    @Test
    void correctFalseAnswers() {
        Question q = new TrueFalseQuestion("Le mont Saint-Michel est en normandie", false, 2);

        assertAll(
                () -> assertEquals(CORRECT, q.tryAnswer("non")),
                () -> assertEquals(CORRECT, q.tryAnswer("Faux")),
                () -> assertEquals(CORRECT, q.tryAnswer("FALSE"))
        );
    }

    @Test
    void incorrectTrueAnswers() {
        Question q = new TrueFalseQuestion("La bretagne fait le meilleur cidre", true,2);

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("non")),
                () -> assertEquals(INCORRECT, q.tryAnswer("toto")),
                () -> assertEquals(INCORRECT, q.tryAnswer("c'est débattable"))
        );
    }

    @Test
    void incorrectFalseAnswers() {
        Question q = new TrueFalseQuestion("La normandie", false,2);

        assertAll(
                () -> assertEquals(INCORRECT, q.tryAnswer("yes")),
                () -> assertEquals(INCORRECT, q.tryAnswer("OK")),
                () -> assertEquals(INCORRECT, q.tryAnswer("true"))
        );
    }
    @Test
    void correctTrueMovieAnswer() {
        Question questionVFMovie = new TrueFalseQuestion("Brad Pitt a joué dans Interview with a Vampire", true,2);
        assertAll(
                () -> assertEquals(CORRECT, questionVFMovie.tryAnswer("oui")),
                () -> assertEquals(CORRECT, questionVFMovie.tryAnswer("vRai")),
                () -> assertEquals(CORRECT, questionVFMovie.tryAnswer("True"))
        );
    }*/
}