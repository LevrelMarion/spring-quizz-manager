package com.zenika.academy.springquizzmanager;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

@Configuration
public class BeanPrinterConfig {
    @Bean
    public File file() {
        File file = new File("src/main/resources/tmdb-api-key.txt");
        return file;
    }
    @Bean
    public Scanner scanner() {
        Scanner scOne = new Scanner(System.in);
        return scOne;
    }
    @Bean
    public TmdbClient tmdbClient() throws IOException {
        File fileApiKey = new File("src/main/resources/tmdb-api-key.txt");
        return new TmdbClient(fileApiKey);
    }

}

