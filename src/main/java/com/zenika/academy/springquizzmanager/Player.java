package com.zenika.academy.springquizzmanager;

/**
 * Represents a player and its score.
 */
public class Player {
    private final String name;
    private Integer score;
    private Integer scoreP;

    public Player(String name) {
        this.name = name;
        this.score = 0;
    }

    /**
     * Add some points to the total score of the player.
     */
    public void scorePoints(int points) {

        this.score += points;
    }

    /**
     * Returns a string that can be displayed to congratulate the player. Includes her score.
     */
    public String congratulations() {
        return "Félicitations, "+name+", vous avez marqué " + score + " points.";
    }

    public int getScoreP() {
        return scoreP;
    }
}
