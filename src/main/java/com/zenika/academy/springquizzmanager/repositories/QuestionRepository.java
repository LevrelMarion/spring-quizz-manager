package com.zenika.academy.springquizzmanager.repositories;
import com.zenika.academy.springquizzmanager.Question;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class QuestionRepository {

    private Map<Long, Question> mapDeQuestions = new HashMap<>();

    public Question getQuestion(long id) {
        return this.mapDeQuestions.get(id);
    }

    public void deletedQuestion(long id) {
        this.mapDeQuestions.remove(id);
    }

    public void save(Question question) {
        this.mapDeQuestions.put(question.getIdQuestion(), question);
    }

    public List<Question> getAll() {
        // methode .values va récupérer la deuxieme colonne value de ma map
        // methode List.copyOf envoie ces valeurs dans une liste et pas une hashmap
        return List.copyOf(mapDeQuestions.values());
    }
}
