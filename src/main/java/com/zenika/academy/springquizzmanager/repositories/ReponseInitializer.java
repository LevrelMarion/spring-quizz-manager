package com.zenika.academy.springquizzmanager.repositories;

import com.zenika.academy.springquizzmanager.Question;

import java.util.Scanner;

public class ReponseInitializer {

    private static int askQuestion(Scanner scOne, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = scOne.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return (2*q.getScore());
            case ALMOST_CORRECT:
                return (q.getScore());
            case INCORRECT:
            default:
                return 0;
        }
    }



}
