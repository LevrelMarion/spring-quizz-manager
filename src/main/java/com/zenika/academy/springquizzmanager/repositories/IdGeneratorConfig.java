package com.zenika.academy.springquizzmanager.repositories;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.atomic.AtomicLong;


@Configuration
public class IdGeneratorConfig {

    private final AtomicLong idQuestion = new AtomicLong();

    @Bean
    public AtomicLong idGenerator() {
        return idQuestion;
    }
}
