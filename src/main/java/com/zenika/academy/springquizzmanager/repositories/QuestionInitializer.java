package com.zenika.academy.springquizzmanager.repositories;
import com.zenika.academy.springquizzmanager.*;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import static com.zenika.academy.springquizzmanager.enums.EnumNiveauQuestions.*;

@Component
public class QuestionInitializer {

    FactoryMovieQuestion factoryMovieQuestion;
    QuestionRepository questionRepository;

    // constucteur
    public QuestionInitializer(FactoryMovieQuestion factoryMovieQuestion, QuestionRepository questionRepository) {
        this.factoryMovieQuestion = factoryMovieQuestion;
        this.questionRepository = questionRepository;
    }
    @PostConstruct
    public void getQuestionTest() {
        //OpenQuestion question2 = new OpenQuestion("Quel est ton nom ?", "marc", 2, 2);
        //questionRepository.save(question2);
        questionRepository.save(factoryMovieQuestion.generateAllLevelOfQuestions(EASY).get());
        questionRepository.save(factoryMovieQuestion.generateAllLevelOfQuestions(MEDIUM).get());
        questionRepository.save(factoryMovieQuestion.generateAllLevelOfQuestions(HARD).get());

    }
}
