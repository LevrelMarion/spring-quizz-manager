package com.zenika.academy.springquizzmanager.repositories;

import com.zenika.academy.springquizzmanager.FactoryMovieQuestion;
import com.zenika.academy.springquizzmanager.Question;
import org.checkerframework.checker.units.qual.A;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ControllerQuestion {

    QuestionRepository questionRepository;

    public ControllerQuestion(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @GetMapping("/questions")
    public List<Question> allQuestions() {
        return questionRepository.getAll();
    }

    @GetMapping("/questions/{id}")
    public Question oneQuestionWithId(@PathVariable long id) {
        return questionRepository.getQuestion(id);
    }
    //@PostMapping("/questions")
    //public Question creerUneQuestion (long id) {
    //}
}
