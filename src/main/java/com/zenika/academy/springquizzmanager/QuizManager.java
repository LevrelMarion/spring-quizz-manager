package com.zenika.academy.springquizzmanager;

import com.zenika.academy.springquizzmanager.questions.MultipleChoiceQuestion;
import com.zenika.academy.springquizzmanager.questions.OpenQuestion;
import com.zenika.academy.springquizzmanager.questions.TrueFalseQuestion;
import com.zenika.academy.springquizzmanager.repositories.QuestionRepository;
import org.springframework.boot.CommandLineRunner;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static com.zenika.academy.springquizzmanager.enums.EnumNiveauQuestions.*;


//@Component
public class QuizManager implements CommandLineRunner {

    FactoryMovieQuestion factoryMovieQuestion;
    Scanner scOne;
    QuestionRepository questionRepository;

    public QuizManager(FactoryMovieQuestion factoryMovieQuestion, QuestionRepository questionRepository) {
        this.factoryMovieQuestion = factoryMovieQuestion;
       // this.scOne = scanner;
        this.questionRepository = questionRepository;
    }

    @Override
    public void run(String... args) throws IOException {

        Random r = new Random();
        Player p = createPlayer(this.scOne);

        List<Question> questions = List.of(
                new OpenQuestion("Comment s'appelle le chien d'Obélix ?", "Idéfix", 1, factoryMovieQuestion.idGenerator.incrementAndGet()),
                new MultipleChoiceQuestion("De quelle couleur est le cheval blanc d'Henri IV ?", List.of("Bleu", "Rouge"), "Blanc", r, 1, factoryMovieQuestion.idGenerator.incrementAndGet()),
                new TrueFalseQuestion("Milou est en fait un chat", false, 1, factoryMovieQuestion.idGenerator.incrementAndGet())
        );
        for (Question q : questions) {
            p.scorePoints(askQuestion(this.scOne, q));
        }
        askQuestion(this.scOne, this.factoryMovieQuestion.generateAllLevelOfQuestions(EASY).get());
        askQuestion(this.scOne, this.factoryMovieQuestion.generateAllLevelOfQuestions(MEDIUM).get());
        askQuestion(this.scOne, this.factoryMovieQuestion.generateAllLevelOfQuestions(HARD).get());

        System.out.println(p.congratulations());
    }

    private static int askQuestion(Scanner scOne, Question q) {
        System.out.println(q.getDisplayableText());
        String userAnswer = scOne.nextLine();
        switch (q.tryAnswer(userAnswer)) {
            case CORRECT:
                return (2*q.getScore());
            case ALMOST_CORRECT:
                return (q.getScore());
            case INCORRECT:
            default:
                return 0;
        }
    }

    private static Player createPlayer(Scanner scOne) {
        System.out.println("Quel est votre nom ?");
        String userName = scOne.nextLine();
        return new Player(userName);
    }
}

