package com.zenika.academy.springquizzmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringQuizzManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringQuizzManagerApplication.class, args);
	}

}
