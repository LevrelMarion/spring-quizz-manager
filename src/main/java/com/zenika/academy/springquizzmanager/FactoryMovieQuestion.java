package com.zenika.academy.springquizzmanager;
import com.zenika.academy.springquizzmanager.enums.EnumNiveauQuestions;
import com.zenika.academy.springquizzmanager.questions.MultipleChoiceQuestion;
import com.zenika.academy.springquizzmanager.questions.OpenQuestion;
import com.zenika.academy.springquizzmanager.questions.TrueFalseQuestion;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class FactoryMovieQuestion {

    //Variable d'instance
    public TmdbClient tmdbClient;
    public Random random;
    public AtomicLong idGenerator;

    //Constructeur
    public FactoryMovieQuestion(TmdbClient tmdbClient, AtomicLong idGenerator) {
        this.tmdbClient = tmdbClient;
        this.idGenerator = idGenerator;
        this.random = new Random();
    }

    public Optional<Question> generateOpenQuestion() {
        Optional<MovieInfo> movie = tmdbClient.getRandomPopularMovie();
        return movie.map((MovieInfo movieInfo) -> getQuestionMovie(movieInfo));
    }

    public Question getQuestionMovie(MovieInfo movieInfo) {
        return new OpenQuestion("Quel est le film sorti en " + movieInfo.year + " dans lequel les acteurs "
                + movieInfo.cast.get(1).actorName +" , "+ movieInfo.cast.get(2).actorName +
                " et "+ movieInfo.cast.get(3).actorName +" ont joués ? ", movieInfo.title, 3, idGenerator.incrementAndGet());
    }

    public Optional<Question> generateVFQuestion() {
        Optional<MovieInfo> movieTF = tmdbClient.getRandomPopularMovie();
        return movieTF.map((MovieInfo movieInfo) -> getTrueFalseMovieQuestion(movieInfo));
    }

    public Question getTrueFalseMovieQuestion(MovieInfo movieInfo) {
        return new TrueFalseQuestion(movieInfo.cast.get(0).actorName +
                " a-t'il joué dans " + movieInfo.title + " ?", true, 1, idGenerator.incrementAndGet());
    }

    public Optional<Question> generateMultipleChoicesQuestion() {
        Optional<MovieInfo> movieTF = tmdbClient.getRandomPopularMovie();
        return movieTF.map((MovieInfo movieInfo) -> getMultipleChoiceMovieQuestion(movieInfo));
    }

    public Question getMultipleChoiceMovieQuestion(MovieInfo movieInfo) {
        return new MultipleChoiceQuestion("Dans quel film a joué " + movieInfo.cast.get(0).actorName + " ? ",
                List.of(tmdbClient.getRandomPopularMovie().get().title,
                        tmdbClient.getRandomPopularMovie().get().title,
                        tmdbClient.getRandomPopularMovie().get().title),
                movieInfo.title, random, 2,
                idGenerator.incrementAndGet());
    }

    public Optional<Question> generateAllLevelOfQuestions(EnumNiveauQuestions enumNiveauQuestions) {
        switch (enumNiveauQuestions) {
            case EASY:
                return generateVFQuestion();
            case MEDIUM:
                return generateOpenQuestion();
            case HARD:
                return generateMultipleChoicesQuestion();
        }
        return Optional.empty();
    }
    public Optional<Question> generateActorFromTitle(String title) {
        Optional<MovieInfo> movieTwo = tmdbClient.getMovie(title);
        return movieTwo.map(this::getQuestionActor);
    }

    public Optional<Question> generatePersonnageFromTitle(String title) {
        Optional<MovieInfo> movieThree = tmdbClient.getMovie(title);
        return movieThree.map(this::getQuestionPersonnage);
    }
    public Question getQuestionActor(MovieInfo movieInfo) {
        return new OpenQuestion("Quel acteur joue le personnage de " + movieInfo.cast.get(0).characterName + " dans le film "
                + movieInfo.title + " ?", movieInfo.cast.get(0).actorName, 1, idGenerator.incrementAndGet());
    }

    public Question getQuestionPersonnage(MovieInfo movieInfo) {
        return new OpenQuestion("Quel est le personnage interprété par " + movieInfo.cast.get(1).actorName + " dans le film "
                + movieInfo.title + " ?", movieInfo.cast.get(1).characterName, 1, idGenerator.incrementAndGet());
    }

}

